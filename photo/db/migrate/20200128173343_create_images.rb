class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.binary :img
      t.string :body
      t.string :picture_id
      t.string :user_id
      t.timestamps
    end
  end
end
