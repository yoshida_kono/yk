class CreatePictures < ActiveRecord::Migration[5.2]
  def change
    create_table :pictures do |t|
      t.string :title
      t.binary :file
      t.string :info
      t.string :user_id

      t.timestamps
    end
  end
end
