class CommentsController < ApplicationController

    
    def create 
       @picture = Picture.find(params[:picture_id])
       @comment = @picture.comments.build(message: params[:comment][:message])
       @comment.user_id = current_user.id
     if @comment.save
      redirect_to picture_path(@picture)
     else
      redirect_to root_path
     end
    end
    
    def destroy
     @picture = Picture.find(params[:picture_id])
     @comment = @picture.comments.find(params[:id])
     @comment.destroy 
     redirect_to picture_path(@picture)
    end
end
