class TopController < ApplicationController
    def main
        @pictures = Picture.all
    end
    
    def login
        user = User.authenticate(params[:name], params[:password])
         if user
          session[:login_uid] = params[:name]
          redirect_to root_path
          flash[:success] = "ログインしました"
         else
          render top_login_path
          flash[:error] = "ユーザー名またはパスワードが違います"
          
         end
    end
    
    def logout
        session.delete(:login_uid)
        redirect_to root_path
        flash[:out] = "ログアウトしました"
    end
end
