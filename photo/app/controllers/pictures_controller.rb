class PicturesController < ApplicationController
    def index
        @pictures = Picture.all
    end
    
    def new
       @picture = Picture.new 
    end
    
    def create
     user_id = User.find_by(name: session[:login_uid]).id
     file = params[:picture][:file].read
     @picture = Picture.new(title: params[:picture][:title], file: file, info: params[:picture][:info], user_id: user_id)
     if @picture.save
      redirect_to root_path
      flash[:post] = "投稿しました"
     else
      render new_picture_path
      flash[:failed] = "投稿に失敗しました"
     end
    end
    def show
        @picture = Picture.find(params[:id])
        @comments = @picture.comments
        @comment = Comment.new
        @images = @picture.images
        @image = Image.all
    end
    
    def destroy
     picture = Picture.find(params[:id])
     picture.destroy
     redirect_to root_path
     flash[:delete] = "投稿を削除しました"
    end
    
    def edit
        @picture = Picture.find(params[:id])
    end
    
    def update
        @picture = Picture.find(params[:id])
        file = params[:picture][:file].read
        @picture.update(title: params[:picture][:title], file: file, info: params[:picture][:info])
        if @picture.save
            flash[:update] = "更新しました！"
            redirect_to root_path
        else
            flash[:fail] = "更新に失敗しました。"
            render :edit
        end
    end
    def get_pic
       picture = Picture.find(params[:id])
       send_data picture.file, disposition: :inline, type:'image/png'
    end

end
