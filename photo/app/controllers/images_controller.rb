class ImagesController < ApplicationController
    def create
     user_id = User.find_by(name: session[:login_uid]).id
     @picture = Picture.find(params[:picture_id])
     img = params[:image][:img].read
     @image = @picture.images.new(img: img, body: params[:image][:body], user_id: user_id)
     if @image.save
      redirect_to picture_path(@picture)
      flash[:post] = "投稿しました"
     else
      render new_image_path
      flash[:failed] = "投稿に失敗しました"
     end
    end
    
    def destroy
     @picture = Picture.find(params[:picture_id])
     @image = @picture.images.find(params[:id])
     @image.destroy
     redirect_to picture_path(@picture)
     flash[:delete] = "投稿を削除しました"
    end
   
    def get_img
       
       image = @picture.images.find(params[:id])
       send_data image.img, disposition: :inline, type:'image/png'
    end
end
