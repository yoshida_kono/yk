class UsersController < ApplicationController
    def index
        @user = User.all
        @pictures = current_user.pictures.all
    end
    
    def new
        @user = User.new
    end
    
    def create
      @user = User.new(name: params[:user][:name],
      password_i: params[:user][:password_i],
      password_i_confirmation: params[:user][:password_i_confirmation])
      if @user.save
         redirect_to top_login_path
      else
         render 'new'
      end
    end
    
    def show
        @user = User.find(params[:id])
    end
    
    def destroy
      user = User.find(params[:id])
      user.destroy
      redirect_to users_path
    end
  
end
