class Picture < ApplicationRecord
    validates :info, presence: true, length: {maximum: 140}
    validates :file, presence: true
    validates :title, presence: true
    validates :user_id, presence: true
    belongs_to :user
    has_many :images, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :comment_users, through: :comments, source: :user
    has_many :image_users, through: :images, source: :user
    has_many :likes, dependent: :destroy
    has_many :like_users, source: :user, through: :likes
    def liked_by?(user)
        likes.where(user_id: user.id).exists?
    end
    def like(user)
    likes.create(user_id: user.id)
    end
  
    def unlike(user)
      likes.find_by(user_id: user.id).destroy
    end
  
    def liked?(user)
      like_user.include?(user)
    end
end
