class User < ApplicationRecord
    validates :password_i, presence: true, confirmation: true
    validates :name, presence: true, uniqueness: true
    
    has_many :pictures, dependent: :destroy
    has_many :images, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :comment_pictures, through: :comments, source: :picture
    has_many :image_pictures, through: :images, source: :picture
    attr_accessor :password_i, :password_i_confirmation
    has_many :likes, dependent: :destroy
    has_many :like_pictures, source: :picture, through: :likes
    has_many :like_images, source: :image, through: :likes
   
    def password_i=(val)
     if val.present?
       self.password = BCrypt::Password.create(val)
     end
     @password_i = val
#     byebug
    end
  
    def self.authenticate(name, pass)
    user = User.find_by(name: name)
    if user and BCrypt::Password.new(user.password) == pass
      user
    else
      nil
    end

    end
end
