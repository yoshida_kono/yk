class Comment < ApplicationRecord
    validates :message, presence: true, length: {maximum: 140}
    validates :user_id, presence: true
    belongs_to :picture
    belongs_to :user
end
