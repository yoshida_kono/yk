class Image < ApplicationRecord
    validates :body, presence: true, length: {maximum: 140}
    validates :img, presence: true
    validates :user_id, presence: true
    belongs_to :user
    belongs_to :picture
    has_many :likes, dependent: :destroy
    has_many :like_users, source: :user, through: :likes
    def liked_by?(user)
        likes.where(user_id: user.id).exists?
    end
  def like(user)
    likes.create(user_id: user.id)
  end
  
  def unlike(user)
    likes.find_by(user_id: user.id).destroy
  end
  
  def liked?(user)
    like_users.include?(user)
  end
    
end
