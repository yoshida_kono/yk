Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'top#main'
  get 'top/login'
  post 'top/login'
  get 'top/logout'
  
  resources :pictures do
    
    member do 
      get "get_pic"
    end
    resources :comments, only: [:index, :new, :create, :destroy]
    resources :images, only: [:create, :destroy] do
     member do 
      get "get_img"
     end
    end
    
  end
  resources :likes, only: [:create, :destroy]
  resources :users, only: [:index, :new, :create, :show, :destroy]
end
